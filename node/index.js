var app = require('express')();
var http = require('http').Server(app);
var io = require('socket.io')(http);
var mysql = require('mysql');

// Se realiza la conexion a la base de datos
var connection = mysql.createConnection({
    host     : 'localhost',
    user     : 'root',
    password : '',
    database : 'maraton'
});

connection.connect(function(err) {
    if (err) {
        console.error('error connecting: ' + err.stack);
        return;
    }
    //console.log('connected as id ' + connection.threadId);
});

io.on('connection', function(socket) {

    socket.on('mostrar turno', function(data) {
        mostrarTurno(data);
    });

    socket.on('tomar pregunta', function(data){

        connection.query({
            sql: 'SELECT id_pregunta FROM preguntas ORDER BY id_pregunta DESC LIMIT 1;',
            timeout: 40000 // 40s
        }, function (error, rows) {
            var pregunta = Math.floor((Math.random() * rows[0].id_pregunta) + 1);

            connection.query({
                sql: 'SELECT pre.id_pregunta, cat.categoria ' +
                        'FROM preguntas pre ' +
                        'INNER JOIN categorias cat ' +
                        'ON pre.categorias_id_categorias = cat.id_categorias ' +
                        'WHERE pre.id_pregunta = ?;',
                timeout: 40000, // 40s
                values: [pregunta]
            }, function (errores, filas) {
                io.emit('tomar pregunta', filas[0]);
            });

        });

    });

    socket.on('seleccionar pregunta', function(data){

        connection.query({
            sql: 'SELECT pre.id_pregunta, pre.pregunta, res.id_respuestas, res.respuesta ' +
                    'FROM preguntas pre ' +
                    'RIGHT JOIN respuestas res ' +
                    'ON pre.id_pregunta = res.id_pregunta ' +
                    'WHERE pre.id_pregunta = ?',
            timeout: 40000, // 40s
            values: [data]
        }, function (error, rows) {
            //console.log(rows);
            io.emit('seleccionar pregunta', rows);
        });

    });

    socket.on('checar jugador turno', function(data){

        connection.query({
            sql: 'SELECT jugador_turno FROM tablero WHERE codigo_tablero = ?;',
            timeout: 40000, // 40s
            values: [data]
        }, function (error, rows) {
            connection.query({
                sql: "SELECT nombre_jugador FROM jugadores WHERE codigo_tablero = ? AND numero_jugador = ?;",
                timeout: 40000, // 40s
                values: [data, rows[0].jugador_turno]
            }, function (errores, filas) {
                io.emit('checar jugador turno', [ rows[0].jugador_turno, filas[0].nombre_jugador]);
            });
        });

    });

    socket.on('validar pregunta', function(data){

        connection.query({
            sql: 'SELECT respuesta_correcta FROM respuestas WHERE id_respuestas = ? AND id_pregunta = ?;',
            timeout: 40000, // 40s
            values: [data.respuesta, data.pregunta]
        }, function (error, rows) {
            if(rows[0].respuesta_correcta !== 1) {
                respuestaIncorrecta();
            } else {
                respuestaCorrecta(data.tablero, data.jugador);
            }
        });

    });

    socket.on('actualizar posicion jugador', function(data){

        if(data.posicion_x >= 1115) {
            data.posicion_tablero = "vertical1";
        }

        connection.query({
            sql: "UPDATE jugadores SET datos_jugador = COLUMN_ADD(datos_jugador, 'posicion_x', ?), datos_jugador = COLUMN_ADD(datos_jugador, 'posicion_y', ?), datos_jugador = COLUMN_ADD(datos_jugador, 'posicion_tablero', ?) WHERE numero_jugador = ? AND codigo_tablero = ?;",
            timeout: 40000, // 40s
            values: [data.posicion_x, data.posicion_y, data.posicion_tablero, data.jugador, data.tablero]
        }, function (error, rows) {
            //console.log("Se actualiza el tablero");
            actualizarTurno(data.tablero, parseInt(data.jugador));
        });

    });

    function actualizarTurno(tablero, jugador) {
        connection.query({
            sql: 'SELECT numero_jugador as ultimo FROM jugadores WHERE codigo_tablero = ? ORDER BY numero_jugador DESC LIMIT 1;',
            timeout: 40000, // 40s
            values: [tablero]
        }, function (error, rows) {

            var turno = 0;

            if(parseInt(rows[0].ultimo) == jugador) {
                turno = 1;
            }

            connection.query({
                sql: 'UPDATE tablero SET jugador_turno = ? WHERE codigo_tablero = ?;',
                timeout: 40000, // 40s
                values: [turno, tablero]
            }, function (errores, filas) {
                //console.log("Turno actualizado");
                mostrarTurno(tablero);
                //io.emit('mostrar turno', filas[0]);
            });
        });
    }

    function mostrarTurno(data) {
        connection.query({
            sql: 'SELECT jugador_turno FROM tablero WHERE codigo_tablero = ?;',
            timeout: 40000, // 40s
            values: [data]
        }, function (error, rows) {

            connection.query({
                sql: 'SELECT nombre_jugador FROM jugadores WHERE codigo_tablero = ? AND numero_jugador = ?;',
                timeout: 40000, // 40s
                values: [data, rows[0].jugador_turno]
            }, function (errores, filas) {
                io.emit('mostrar turno', filas[0]);
            });
        });
    }

    function respuestaCorrecta(tablero, jugador) {
        connection.query({
            sql: "SELECT COLUMN_GET(datos_jugador, 'posicion_x' as char) AS posicion_x, COLUMN_GET(datos_jugador, 'posicion_y' as char) AS posicion_y, COLUMN_GET(datos_jugador, 'posicion_tablero' as char) AS posicion_tablero FROM jugadores WHERE codigo_tablero = ? AND numero_jugador = ?;",
            timeout: 40000, // 40s
            values: [tablero, jugador]
        }, function (errores, filas) {

            if(filas[0].posicion_tablero == "horizontal1") {
                filas[0].posicion_x = parseInt(filas[0].posicion_x) + 130;
            }

            //filas[0].posicion_y = parseInt(filas[0].posicion_y);

            io.emit('respuesta correcta', { posicion_x: filas[0].posicion_x, posicion_y: filas[0].posicion_y, posicion_tablero: filas[0].posicion_tablero, jugador: parseInt(jugador) });
        });
    }

    function respuestaIncorrecta() {
        console.log("Respuesta Incorrecta");
        //connection.query({
        //    sql: "UPDATE jugadores SET datos_jugador = COLUMN_ADD(datos_jugador, 'respuesta_correcta', 0) WHERE id_jugador = ?;",
        //    timeout: 40000, // 40s
        //    values: [data, rows[0].jugador_turno]
        //}, function (errores, filas) {
        //    io.emit('checar jugador turno', filas[0]);
        //});
    }

});

http.listen(3000, function(){
    console.log('listening on *:3000');
});