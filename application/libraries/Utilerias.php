<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Utilerias {

    function __construct() {
        $this->ci =& get_instance();
    }

    function generarCadenaRandom($longitud) {
        return substr(str_shuffle("0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ"), 0, $longitud);
    }
}