<script src="<?= base_url("node/node_modules/socket.io/node_modules/socket.io-client/socket.io.js") ?>"></script>
<script src="<?= base_url("assets/js/jquery.min.js") ?>"></script>
<script src="<?= base_url("assets/js/bootstrap.min.js") ?>"></script>
<script src="<?= base_url("assets/js/sweetalert.min.js") ?>"></script>
<script>
    var socket = io(url_socket);
</script>