<!-- Bootstrap core CSS -->
<link href="<?= base_url("assets/css/bootstrap.min.css") ?>" rel="stylesheet">

<!--Sweet Alert-->
<link href="<?= base_url("assets/css/sweetalert.css") ?>" rel="stylesheet">

<!--    Estilos propios-->
<link href="<?= base_url("assets/css/estilos.css") ?>" rel="stylesheet">