<div class="container">

    <div class="starter-template">
        <input type="hidden" name="id_tablero" id="id_tablero" value="<?= $tablero ?>">
        <h1>Bienvenido al maraton</h1>
        <p class="lead">Por favor, accede al siguiente link <strong><?= site_url("jugadores") ?></strong> con la clave de abajo, para poder empezar a jugar.</p>
        <h1><?= $tablero ?></h1>
        <div>
            <ul id="jugadores">
            </ul>
        </div>
    </div>

</div><!-- /.container -->

<?php include "application/views/includes/js_include.php"; ?>

<script>

    function revisarJugadores() {
        var tablero = $("#id_tablero").val();

        $.ajax({
            url: js_base_url("main/checarJugadoresListos"),
            method: 'POST',
            dataType: 'json',
            data: {tablero: tablero},
            success: function (s) {
                if(s) {
                    location.href = js_base_url("main/iniciarJuego/"+tablero);
                }
            },
            error: function (e) {
                console.log(e.responseText);
            }
        });

    }

    function mostrarJugadoresRegistraados() {
        var tablero = $("#id_tablero").val();

        $.ajax({
            url: js_base_url('main/tomarJugadoresRegistrados'),
            method: 'POST',
            dataType: 'json',
            data: {tablero: tablero}
        })
            .done(function( data ) {
                var jugadores = "";
                for(var i = 0; i < data.length; i++){
                    jugadores += "<li>"+data[i].nombre_jugador+"</li>";
                }
                $("#jugadores").html(jugadores);
            })
            .fail(function(e) {
                console.log(e.responseText);
            });
    }

    setInterval(revisarJugadores, 2000);
    setInterval(mostrarJugadoresRegistraados, 2000);

</script>
</body>
</html>