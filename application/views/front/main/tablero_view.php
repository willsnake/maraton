<div class="container">

    <div class="starter-template">
        <input type="hidden" name="id_tablero" id="id_tablero" value="<?= $tablero ?>">
        <div id="tablero"></div>
    </div>

</div><!-- /.container -->

<div class="modal fade" tabindex="-1" id="modal_pregunta" role="dialog" aria-labelledby="myLargeModalLabel">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="encabezado_respuestas"></h4>
            </div>
            <div class="modal-body">
                <div id="respuestas_div"></div>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" tabindex="-1" id="modal_turno" role="dialog" aria-labelledby="myLargeModalLabel">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="encabezado_respuestas"></h4>
            </div>
            <div class="modal-body">
                <div id="respuestas_div"></div>
            </div>
        </div>
    </div>
</div>

<?php include "application/views/includes/js_include.php"; ?>

<script>
    var tablero = $("#id_tablero").val();

    $(window).on("load ", function(){
        swal({
                title: "Preparados",
                text: "En sus marcas, listos...",
                timer: 5000,
                showConfirmButton: false,
                imageUrl: "<?= base_url("assets/img/linea_inicio.jpg") ?>"
            },
            function(){
                mostrarTurno();
            }
        );
    });

    function mostrarTurno() {
        socket.emit('mostrar turno', tablero);
    }

    socket.on('mostrar turno', function(data){
        swal({
                title: data.nombre_jugador,
                text: "Es tu turno",
                timer: 3500,
                showConfirmButton: false,
                animation: "slide-from-top",
                imageUrl: "<?= base_url("assets/img/linea_inicio.jpg") ?>"
            },
            function(){
                mostrarDado();
            }
        );
    });

    function mostrarDado() {

        socket.emit('tomar pregunta', tablero);

        socket.on('tomar pregunta', function(data){
            swal({
                    title: "Categoria",
                    timer: 5000,
                    text: '<h3 id="categoria">'+data.categoria+'</h3>',
                    showConfirmButton: false,
                    imageUrl: "<?= base_url("assets/img/dado.gif") ?>",
                    html: true
                },
                function(){
                    mostrarPregunta(data.id_pregunta);
                }
            );
        });

    }

    function mostrarPregunta(id) {
        socket.emit('seleccionar pregunta', id);

        socket.on('seleccionar pregunta', function(data){
            $('#modal_pregunta').modal('show');
            $("#respuestas_div").html(
                '<ul>' +
                    '<li><img src="<?= base_url("assets/img/boton_Y.png") ?>" class="img-circle" style="width: 10%;" /> '+data[0].respuesta+'</li>' +
                    '<li><img src="<?= base_url("assets/img/boton_X.png") ?>" class="img-circle" style="width: 10%;" /> '+data[1].respuesta+'</li>' +
                    '<li><img src="<?= base_url("assets/img/boton_B.png") ?>" class="img-circle" style="width: 10%;" /> '+data[2].respuesta+'</li>' +
                    '<li><img src="<?= base_url("assets/img/boton_A.png") ?>" class="img-circle" style="width: 10%;" /> '+data[3].respuesta+'</li>' +
                '</ul>'
            );
        });

    }


    var jugador1Listo = false;
    var imagenJugador1 = new Image();

    var jugador2Listo = false;
    var imagenJugador2 = new Image();

    var jugador3Listo = false;
    var imagenJugador3 = new Image();

    var jugador4Listo = false;
    var imagenJugador4 = new Image();

    var cantidadJugadores = "<?= $jugadores ?>";

    var canvas = document.createElement("canvas");
    var ctx = canvas.getContext("2d");
    canvas.width = 1070;
    canvas.height = 724;
    canvas.style.border = "black 1px solid";
    document.getElementById("tablero").appendChild(canvas);

    socket.on('respuesta correcta', function(data){

        swal({
                title: "Respuesta corecta",
                timer: 3500,
                type: "success",
                showConfirmButton: false,
                animation: "slide-from-top"
            }
        );

        setTimeout(function(){
            console.log(data);
            animate(data.jugador, data.posicion_x, data.posicion_y);
            socket.emit('actualizar posicion jugador', { posicion_x: data.posicion_x, posicion_y: data.posicion_y, posicion_tablero: data.posicion_tablero, jugador: data.jugador, tablero: tablero } );
        },3500);

    });

    // Background
    var bgReady = false;
    var bgImage = new Image();
    bgImage.onload = function () {
        bgReady = true;
    };
    bgImage.src = "<?= base_url("assets/img/tablero.png") ?>";

    // Ignorancia
    var ignoranciaLista = false;
    var imagenIgnorancia = new Image();
    imagenIgnorancia.onload = function () {
        ignoranciaLista = true;
    };
    imagenIgnorancia.src = "<?= base_url("assets/img/ignorancia.png") ?>";

    switch(cantidadJugadores) {
        case "1":
            imagenJugadorUno();
        break;
        case "2":
            imagenJugadorUno();
            imagenJugadorDos();
        break;
        case "3":
            imagenJugadorUno();
            imagenJugadorDos();
            imagenJugadorTres();
        break;
        case "4":
            imagenJugadorUno();
            imagenJugadorDos();
            imagenJugadorTres();
            imagenJugadorCuatro();
        break;
    }

    function imagenJugadorUno(){
        // Jugador 1
        imagenJugador1.onload = function () {
            jugador1Listo = true;
        };
        imagenJugador1.src = "<?= base_url("assets/img/pieza_1.png") ?>";
    }

    var jugador1 = {
        speed: 256,
        x: 75,
        y: 45
    };

    function imagenJugadorDos(){
        // Jugador 2
        imagenJugador2.onload = function () {
            jugador2Listo = true;
        };
        imagenJugador2.src = "<?= base_url("assets/img/pieza_2.png") ?>";
    }

    var jugador2 = {
        speed: 256,
        x: 75,
        y: 70
    };

    function imagenJugadorTres(){
        // Jugador 3
        imagenJugador3.onload = function () {
            jugador3Listo = true;
        };
        imagenJugador3.src = "<?= base_url("assets/img/pieza_3.png") ?>";
    }

    var jugador3 = {
        speed: 256,
        x: 75,
        y: 95
    };

    function imagenJugadorCuatro(){
        // Jugador 4
        imagenJugador4.onload = function () {
            jugador4Listo = true;
        };
        imagenJugador4.src = "<?= base_url("assets/img/pieza_4.png") ?>";
    }

    var jugador4 = {
        speed: 256,
        x: 75,
        y: 117
    };

    var ignorancia = {
        speed: 256,
        x: 75,
        y: 140
    };

    function animate(jugadorInt, limiteX, limiteY) {

        switch (jugadorInt) {
            case 1:
                jugador = jugador1;
                break;
            case 2:
                jugador = jugador2;
                break;
            case 3:
                jugador = jugador3;
                break;
            case 4:
                jugador = jugador4;
                break;
        }

        var x = jugador.x;
        var y = jugador.y;

        for (x; x <= limiteX; x++) {
            jugador.x += 1;
        }

        for (y; y <= limiteY; y++) {
            jugador.y += 1;
        }

    }

    // Draw everything
    var render = function () {
        if (bgReady) {
            ctx.drawImage(bgImage, 0, 0);
        }

        switch(cantidadJugadores) {
            case "1":
                if (jugador1Listo) {
                    ctx.drawImage(imagenJugador1, jugador1.x, jugador1.y, 10, 20);
                }
                break;
            case "2":
                if (jugador1Listo) {
                    ctx.drawImage(imagenJugador1, jugador1.x, jugador1.y, 10, 20);
                }
                if (jugador2Listo) {
                    ctx.drawImage(imagenJugador2, jugador2.x, jugador2.y, 10, 20);
                }
                break;
            case "3":
                if (jugador1Listo) {
                    ctx.drawImage(imagenJugador1, jugador1.x, jugador1.y, 10, 20);
                }
                if (jugador2Listo) {
                    ctx.drawImage(imagenJugador2, jugador2.x, jugador2.y, 10, 20);
                }
                if (jugador3Listo) {
                    ctx.drawImage(imagenJugador3, jugador3.x, jugador3.y, 10, 20);
                }
                break;
            case "4":
                if (jugador1Listo) {
                    ctx.drawImage(imagenJugador1, jugador1.x, jugador1.y, 10, 20);
                }
                if (jugador2Listo) {
                    ctx.drawImage(imagenJugador2, jugador2.x, jugador2.y, 10, 20);
                }
                if (jugador3Listo) {
                    ctx.drawImage(imagenJugador3, jugador3.x, jugador3.y, 10, 20);
                }
                if (jugador4Listo) {
                    ctx.drawImage(imagenJugador4, jugador4.x, jugador4.y, 10, 20);
                }
                break;
        }

        if (ignoranciaLista) {
            ctx.drawImage(imagenIgnorancia, ignorancia.x, ignorancia.y, 10, 20);
        }
    };

    // The main game loop
    var main = function () {
        var now = Date.now();
        var delta = now - then;

//        animate(delta / 1000);
        render();

//        then = now;

        // Request to do this again ASAP
        requestAnimationFrame(main);
    };

    // Cross-browser support for requestAnimationFrame
    var w = window;
    requestAnimationFrame = w.requestAnimationFrame || w.webkitRequestAnimationFrame || w.msRequestAnimationFrame || w.mozRequestAnimationFrame;

    // Let's play this game!
    var then = Date.now();
//    reset();
    main();

</script>
</body>
</html>