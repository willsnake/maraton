<div class="container">

    <div class="starter-template">
        <h1>Bienvenido al maraton</h1>
        <p class="lead">Por favor, ingresa el codigo del tablero:</p>
        <form action="#" method="POST" id="forma_registro">
            <div class="form-group">
                <label for="codigo_tablero">Codigo de Tablero</label>
                <input type="text" class="form-control" name="codigo_tablero" id="codigo_tablero" placeholder="Codigo del Tablero" style="text-transform: uppercase;" required/>
            </div>
            <div class="form-group">
                <label for="usuario">Usuario</label>
                <input type="text" class="form-control" name="usuario" id="usuario" placeholder="Usuario" required/>
            </div>
            <button type="submit" id="usuario_entrar" class="btn btn-default">Entrar</button>
        </form>
        <input type="hidden" name="tablero_oculto" id="tablero_oculto" value="0" />
        <button type="submit" id="jugadores_listos" class="btn btn-default" style="display: none;">Todos los jugadores estamos listos!</button>
    </div>

</div><!-- /.container -->

<?php include "application/views/includes/js_include.php"; ?>

<script>
    var tablero_global = 0;

    $("#usuario_entrar").on("click", function(e) {
        e.preventDefault();
        var map = {};
        $(":input").each(function() {
            map[$(this).attr("name")] = $(this).val();
        });

        $.ajax({
            url: js_base_url('jugadores/insertarJugador'),
            method: 'POST',
            dataType: 'json',
            data: map
        })
            .done(function( data ) {
                if(data.status === 200)
                {
                    $("#tablero_oculto").val(data.tablero);
                    $("#forma_registro").fadeOut();
                    $("#jugadores_listos").fadeIn();
                }
                else
                {
                    swal({
                        title: "Error!",
                        text: data.mensaje,
                        type: "error",
                        confirmButtonText: "Ok"
                    });
                }
            })
            .fail(function(e) {
                console.log(e.responseText);
            });
    });

    $("#jugadores_listos").on("click", function(e) {
        e.preventDefault();
        var tablero = $("#tablero_oculto").val();

        $.ajax({
            url: js_base_url('jugadores/jugadoresListos'),
            method: 'POST',
            dataType: 'json',
            data: { tablero: tablero }
        })
            .done(function( data ) {
                if(data.status === 200) {
                    tablero_global = 200;
                    location.href= js_base_url('jugadores/tableroRespuestas');
                }
                else
                {
                    swal({
                        title: "Error!",
                        text: data.mensaje,
                        type: "error",
                        confirmButtonText: "Ok"
                    });
                }
            })
            .fail(function(e) {
                console.log(e.responseText);
            });
    });

    function checarTablero() {
        var tablero = $("#tablero_oculto").val();

        $.ajax({
            url: js_base_url('main/checarJugadoresListosGlobal'),
            method: 'POST',
            dataType: 'json',
            data: { tablero: tablero }
        })
            .done(function( data ) {
                if(data.status === 200) {
                    location.href= js_base_url('jugadores/tableroRespuestas');
                }
            })
            .fail(function(e) {
                console.log(e.responseText);
            });
    }

    setInterval(checarTablero, 2000);
</script>
</body>
</html>