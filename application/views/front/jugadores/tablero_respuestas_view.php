<div class="container">

    <div class="starter-template" style="display: none;" id="botones_respuesta">
        <div class="row">
            <div class="col-xs-3 col-sm-3 col-md-3"></div>
            <div class="col-xs-6 col-sm-6 col-md-6">
                <img id="respuestaY" src="<?= base_url("assets/img/boton_Y.png") ?>" class="img-circle boton-responder-singular"/>
                <input type="hidden" id="respuestaY_hidden" name="respuestaY_hidden" value="" />
            </div>
            <div class="col-xs-3 col-sm-3 col-md-3"></div>
        </div>
        <div class="row">
            <div class="col-xs-3 col-sm-3 col-md-3"></div>
            <div class="col-xs-3 col-sm-3 col-md-3">
                <img id="respuestaX" src="<?= base_url("assets/img/boton_X.png") ?>" class="img-circle boton-responder-plural" />
                <input type="hidden" id="respuestaX_hidden" name="respuestaX_hidden" value="" />
            </div>
            <div class="col-xs-3 col-sm-3 col-md-3">
                <img id="respuestaB" src="<?= base_url("assets/img/boton_B.png") ?>" class="img-circle boton-responder-plural" />
                <input type="hidden" id="respuestaB_hidden" name="respuestaB_hidden" value="" />
            </div>
            <div class="col-xs-3 col-sm-3 col-md-3"></div>
        </div>
        <div class="row">
            <div class="col-xs-3 col-sm-3 col-md-3"></div>
            <div class="col-xs-6 col-sm-6 col-md-6">
                <img id="respuestaA" src="<?= base_url("assets/img/boton_A.png") ?>" class="img-circle boton-responder-singular" />
                <input type="hidden" id="respuestaA_hidden" name="respuestaA_hidden" value="" />
            </div>
            <div class="col-xs-3 col-sm-3 col-md-3"></div>
        </div>
    </div>

    <input type="hidden" name="id_pregunta" id="id_pregunta" value="" />

    <div class="starter-template" id="panel_espera">
        <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-12">
                <div class="panel panel-default">
                    <div class="panel-heading">Espera tu turno</div>
                    <div class="panel-body" id="nombre_jugador_turno">

                    </div>
                </div>
            </div>
        </div>
    </div>
    <p>
        <?php
        $this->debugeo->imprimir_pre($this->session->userdata());
        ?>
    </p>

</div><!-- /.container -->

<?php include "application/views/includes/js_include.php"; ?>

<script>
    var jugador = "<?= $this->session->userdata('numero_jugador'); ?>";
    var tablero = "<?= $this->session->userdata('tablero_jugador'); ?>";

    socket.emit('checar jugador turno', tablero);

    socket.on('checar jugador turno', function(data) {
        $("#nombre_jugador_turno").html("El jugador en turno en este momento es: "+data[0].nombre_jugador);
        if(data[0] == jugador)
        {
            $("#botones_respuesta").fadeIn();
            $("#panel_espera").fadeOut();
        }
        else {
            $("#botones_respuesta").fadeOut();
            $("#panel_espera").fadeIn();
        }
    });

    socket.on('seleccionar pregunta', function(data){
        $("#respuestaY_hidden").val(data[0].id_respuestas);
        $("#respuestaX_hidden").val(data[1].id_respuestas);
        $("#respuestaB_hidden").val(data[2].id_respuestas);
        $("#respuestaA_hidden").val(data[3].id_respuestas);
        $("#id_pregunta").val(data[0].id_pregunta);
    });

    $("#respuestaY").on("click", function() {
        var datos = [$("#respuestaY_hidden").val(), $("#id_pregunta").val(), jugador, tablero];
        validarRespuesta(datos);
    });

    $("#respuestaX").on("click", function() {
        var datos = [$("#respuestaX_hidden").val(), $("#id_pregunta").val(), jugador, tablero];
        validarRespuesta(datos);
    });

    $("#respuestaB").on("click", function() {
        var datos = [$("#respuestaB_hidden").val(), $("#id_pregunta").val(), jugador, tablero];
        validarRespuesta(datos);
    });

    $("#respuestaA").on("click", function() {
        var datos = [$("#respuestaA_hidden").val(), $("#id_pregunta").val(), jugador, tablero];
        validarRespuesta(datos);
    });

    function validarRespuesta(datos) {
        socket.emit('validar pregunta', { respuesta: datos[0], pregunta: datos[1], jugador: datos[2], tablero: datos[3] });
    }

</script>
</body>
</html>