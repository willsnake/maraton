<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Tablero_model extends CI_Model {

    public function __construct() {
        parent::__construct();
    }

    /**
     * Esta funci�n inserta un nuevo tablero en espera de jugadores, con un c�digo aleatorio
     * @param $codigoTablero
     * @return mixed
     */
    public function apartarTablero($codigoTablero) {
        $data = array(
            'codigo_tablero' => $codigoTablero,
        );

        $query = $this->db->insert('tablero', $data);
        return $query;
    }

    /**
     * Esta funci�n revisa si el tablero ya est� listo para poder empezar el juego
     * @param $codigoTablero
     * @return bool
     */
    public function checarJugadoresListos($codigoTablero) {
        $this->db->select('tablero_listo')->from('tablero')->where('codigo_tablero', $codigoTablero);
        $query = $this->db->get();
        $revision = $query->row_array();
        if($revision["tablero_listo"]) {
            return TRUE;
        }
        return FALSE;
    }

    /**
     * Esta funci�n se encarga de marcar que todos los jugadores est�n listos para empezar
     * @param $tablero
     * @return mixed
     */
    public function tableroListo($tablero) {
        $data = array(
            'tablero_listo' => 1,
        );

        $this->db->where('codigo_tablero', $tablero);
        $query = $this->db->update('tablero', $data);
        return $query;
    }

    /**
     * Esta funci�n sirve para saber si el tablero est� listo
     * @param $tablero
     * @return mixed
     */
    public function checarTableroListo($tablero) {
        $this->db->select('tablero_listo')->from('tablero')->where('codigo_tablero', $tablero);
        $query = $this->db->get();
        return $query->row_array();
    }

    /**
     * Esta funci�n devuelve el nombre de las personas registradas conforme al c�digo del tablero
     * @param $tablero
     * @return mixed
     */
    public function tomarJugadores($tablero)
    {
        $this->db->select('nombre_jugador')->from('jugadores')->where('codigo_tablero', $tablero);
        $query = $this->db->get();
        return $query->result_array();
    }

    /**
     * Esta funci�n se encarga de revisar el turno del jugador en el tablero
     * @param $tablero
     * @return mixed
     */
    public function checarTurnoTablero($tablero)
    {
        $sql = "SELECT jugador_turno FROM tablero WHERE codigo_tablero = ?;";
        $query = $this->db->query($sql, array($tablero));
        $jugador = $query->row_array();
        return $jugador["jugador_turno"];
    }

    /**
     * Esta funci�n se encarga de tomar el nombre del jugador en turno del tablero
     * @param $tablero
     * @return mixed
     */
    public function tomarNombreJugadorTurno($tablero, $turno_jugador)
    {
        $sql = "SELECT nombre_jugador FROM jugadores WHERE codigo_tablero = ? AND numero_jugador = ?;";
        $query = $this->db->query($sql, array($tablero, $turno_jugador));
        $jugador = $query->row_array();
        return $jugador["nombre_jugador"];
    }

    public function tomarPregunta($tablero)
    {
        $sql = "SELECT id_pregunta FROM preguntas ORDER BY id_pregunta DESC LIMIT 1;";
        $query = $this->db->query($sql);
        $total_preguntas = $query->row_array();

        do {
            $numero_random = mt_rand(1, $total_preguntas["id_pregunta"]);

            $sql_pregunta = "SELECT pre.id_pregunta, pre.pregunta, pre.codigo_tablero, cat.categoria
                                FROM preguntas pre
                                INNER JOIN categorias cat
                                ON pre.categorias_id_categorias = cat.id_categorias
                                WHERE pre.id_pregunta = ?;";
            $query_pregunta = $this->db->query($sql_pregunta, array($numero_random));
            $pregunta = $query_pregunta->row_array();

        } while ($pregunta["codigo_tablero"] == $tablero);

        $datos_actualizar = array(
            'codigo_tablero' => $tablero,
        );

        $this->db->where('id_pregunta', $numero_random);
        $this->db->update('preguntas', $datos_actualizar);

        return $pregunta;
    }

}