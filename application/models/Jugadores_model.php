<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Jugadores_model extends CI_Model
{

    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Esta funci�n se encarga de insertar un jugador conforme a un c�digo de un tablero
     * @param $datos
     * @return mixed
     */
    public function insertarJugador($datos)
    {

        $sql = "INSERT INTO jugadores (codigo_tablero, nombre_jugador, numero_jugador, datos_jugador) VALUES
                (?, ?, ?,
                COLUMN_CREATE(
                    'posicion_x', ?,
                    'posicion_y', ?,
                    'posicion_tablero', ?,
                    'posicion_ignorancia_x', 75,
                    'posicion_ignorancia_y', 140)
                    );";
        $query = $this->db->query($sql, array(
            strtoupper($datos["codigo_tablero"]),
            $datos["usuario"],
            $datos["numero_jugador"],
            $datos["x"],
            $datos["y"],
            $datos["lugar_tablero"],
        ));
        return $query;

//        $data = array(
//            'codigo_tablero' => strtoupper($datos["codigo_tablero"]),
//            'nombre_jugador' => $datos["usuario"],
//            'numero_jugador' => $datos["numero_jugador"],
//        );
//
//        $query = $this->db->insert('jugadores', $data);
//        return $query;
    }

    /**
     * Esta funci�n se encarga de tomar el �ltimo jugador registrado dentro del tablero conforme a su c�digo
     * @param $cuarto
     * @return int
     */
    public function ultimoJugador($cuarto)
    {
        $sql = "SELECT numero_jugador as ultimo FROM jugadores WHERE codigo_tablero = ? ORDER BY numero_jugador DESC LIMIT 1;";
        $query = $this->db->query($sql, array($cuarto));
        $row = $query->row();
        if(!$row) {
            $row = 0;
        }
        return $row;
    }

}