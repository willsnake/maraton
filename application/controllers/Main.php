<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Main extends CI_Controller {

    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Esta funci�n es la p�gina principal del marat�n
     */
	public function index()
    {
//        Se genera una cadena aleatoria para crear un c�digo de tablero nuevo
        $cadenaRandom = $this->utilerias->generarCadenaRandom(5);
//        Se aparta el tablero y se guarda en espera de jugadores
        $this->tablero_model->apartarTablero($cadenaRandom);

//		Se muestra la clave del tablero a los jugadores que deeseen entrar a jugar
        $arreglo = array(
            "tablero" => $cadenaRandom
        );
		$this->load->view('front/header_view');
		$this->load->view('front/main/inicio_view', $arreglo);
	}

    /**
     * Esta funci�n revisa si los jugadores ya est�n listos
     */
    public function checarJugadoresListos()
    {
//        Se toma el c�digo del tablero, del usuario que hace la petici�n
        $tablero = $this->input->post("tablero", TRUE);
//        Se revisa si los dem�s jugadores est�n listos para jugar
        $revision = $this->tablero_model->checarJugadoresListos($tablero);
//        Se imprime verdadero o falso, dependiendo del resultado
        echo(json_encode($revision));
    }

    /**
     * Esta funci�n muestra el tablero con los jugadores ya registrados
     * @param $tablero
     */
    public function iniciarJuego($tablero)
    {
//        Se toma el valor del �ltimo jugador dentro del tablero de juego
        $ultimo = $this->jugadores_model->ultimoJugador(strtoupper($tablero));

//        Se le indica al tablero la cantidad de jugadores y el tablero que se desea iniciar
        $arreglo = array(
            "tablero" => $tablero,
            "jugadores" => $ultimo->ultimo,
        );
        $this->load->view('front/header_view');
        $this->load->view('front/main/tablero_view', $arreglo);
    }

    /**
     * Esta funci�n se encarga de tomar los jugadores registrados,
     * su su finalidad es mostrar los nombres de los jugadores conforme al c�digo del tablero
     */
    public function tomarJugadoresRegistrados()
    {
//        Se toma el c�digo del tablero
        $tablero = $this->input->post("tablero", TRUE);
//        Se toman los jugadores registrados conforme al c�digo del tablero
        $jugadores = $this->tablero_model->tomarJugadores($tablero);
//        Se imprimen los nombres
        echo(json_encode($jugadores));
    }

    /**
     * Esta funci�n se encarga de revisar si el tablero est� listo para poder empezar a jugar
     */
    public function checarJugadoresListosGlobal() {
        try
        {
//            Se toma el c�digo del tablero
            $tablero = $this->input->post("tablero", TRUE);

//            Se revisa si el tablero est� listo
            $tablero_listo = $this->tablero_model->checarTableroListo($tablero);

//            Si el tablero devuelve un 0, quiere decir que no est� listo y marca un error
            if ($tablero_listo["tablero_listo"] == 0)
            {
                throw new Exception("Error");
            }
//            De lo contrario, devuelve un status 200, que quiere decir que el tablero est� listo
            else
            {
                $respuesta = array(
                    "status" => 200,
                );
                echo(json_encode($respuesta));
            }
        }
        catch (Exception $e)
        {
//            Esta excepcion pasa si es que encuentra un error
            $respuesta = array(
                "mensaje" => $e->getMessage(),
            );
            echo(json_encode($respuesta));
        }
    }

    public function checarTurnoTablero()
    {
//        Se toma el c�digo del tablero
        $tablero = $this->input->post("tablero", TRUE);
        $jugador = $this->tablero_model->checarTurnoTablero($tablero);
        $nombre_jugador = $this->tablero_model->tomarNombreJugadorTurno($tablero, $jugador);
        $respuesta = array(
            "turno_jugador" => $jugador,
            "nombre_jugador" => $nombre_jugador,
        );
        echo(json_encode($respuesta));
    }

    public function seleccionarPregunta()
    {
//        Se toma el c�digo del tablero
        $tablero = $this->input->post("tablero", TRUE);
        $pregunta = $this->tablero_model->tomarPregunta($tablero);

        echo(json_encode($pregunta));

    }
}
