<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Jugadores extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Esta funci�n es la principal para la pantalla de los jugadores
     */
    public function index()
    {
//        Se destruye una sesi�n previa para resetear los datos del jugador
        $this->session->sess_destroy();
//        Se carga la vista de los jugadores
        $this->load->view('front/header_view');
        $this->load->view('front/jugadores/index_jugadores_view');
    }

    /**
     * Esta funci�n se encarga de insertar a un jugador
     */
    public function insertarJugador()
    {
//        Se toman los datos del usuario y se guardan en un arreglo
        $datos = $this->input->post();

//        Se limpian de espacios vaci�s y se guardan en el mismo arreglo
        foreach ($datos as $key => $value)
        {
            $datos[$key]=trim($value);
        }

//        $this->debugeo->imprimir_pre($datos);

        try
        {

//            Se toma el �ltimo jugador registrado dentro del tablero
            $ultimo = $this->jugadores_model->ultimoJugador(strtoupper($datos["codigo_tablero"]));

//            Si el valor del �ltimo jugador es nulo, quiere decir que no hay un jugador registrado previamente
//            en  ese caso se inicializa en 1
            if($ultimo) {
                $datos["numero_jugador"] = $ultimo->ultimo + 1;
            }
            else {
                $datos["numero_jugador"] = 1;
            }

            $datos["lugar_tablero"] = "horizontal1";
            $datos["respuesta_correcta"] = 0;

            switch($datos["numero_jugador"]) {
                case 1:
                    $datos["x"] = 75;
                    $datos["y"] = 45;
                    break;

                case 2:
                    $datos["x"] = 75;
                    $datos["y"] = 70;
                    break;

                case 3:
                    $datos["x"] = 75;
                    $datos["y"] = 95;
                    break;

                case 4:
                    $datos["x"] = 75;
                    $datos["y"] = 117;
                    break;
            }

//            Si el n�mero de jugadores es mayor al n�mero 5, se marca un error indicando que la sala de juegos est� llena
            if ($datos["numero_jugador"] > 4) {
                throw new Exception('La sala esta llena.');
            }

//            De lo contrario, se inserta el jugador dentro del tablero del juego
            $insertarJugador = $this->jugadores_model->insertarJugador($datos);

//            Si no se pudo insertar el jugador, se manda un error al usuario
            if (!$insertarJugador)
            {
                throw new Exception('Hubo un error al tratar de entrar, por favor intente de nuevo.');
            }
//            De lo contrario, se regresa un status 200, lo que quiere decir que la operaci�n fue exitosa
//            y se agregan datos a la sesi�n del usuario para poder identificarlo
            else
            {

                $informacionJugador = array(
                    'jugador' => $datos["usuario"],
                    'numero_jugador' => $datos["numero_jugador"],
                    'tablero_jugador' => $datos["codigo_tablero"],
                );

                $this->session->set_userdata($informacionJugador);

                $respuesta = array(
                    "mensaje" => "Todos los jugadores estan listos",
                    "tablero" => strtoupper($datos["codigo_tablero"]),
                    "status" => 200,
                );
                echo(json_encode($respuesta));
            }
        }
        catch (Exception $e)
        {
//            De lo contrario, se madnda un estatus 300 que indica un error
            $respuesta = array(
                "mensaje" => $e->getMessage(),
                "tablero" => strtoupper($datos["codigo_tablero"]),
                "status" => 300,
            );
            echo(json_encode($respuesta));
        }
    }

    /**
     * Esa funci�n se encarga de marcar que el tablero ya est� listo para comenzar a jugar
     */
    public function jugadoresListos() {
        try
        {
//            Se toma el c�digo del tablero
            $tablero = $this->input->post("tablero", TRUE);

//            Se marca el tablero para indicar que ya est� listo
            $tablero_listo = $this->tablero_model->tableroListo($tablero);

//            Si hay un error, se imprime al usuario
            if (!$tablero_listo)
            {
                throw new Exception('Hubo un error al tratar de iniciar el tablero, por favor intente de nuevo.');
            }
//            De lo contrario, se devuelve un status 200, indicando que se marc� el tablero con �xito
            else
            {
                $respuesta = array(
                    "status" => 200,
                );
                echo(json_encode($respuesta));
            }
        }
        catch (Exception $e)
        {
            $respuesta = array(
                "mensaje" => $e->getMessage(),
            );
            echo(json_encode($respuesta));
        }
    }

    /**
     * Esta funci�n muestra el panel donde los usuarios pueden responder
     */
    public function tableroRespuestas() {
        $this->load->view('front/header_view');
        $this->load->view('front/jugadores/tablero_respuestas_view');
    }

}